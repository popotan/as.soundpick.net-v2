import { BrowserModule } from '@angular/platform-browser';
import { RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { AppComponent } from './app.component';
import { IndexComponent } from './index/index.component';
import { AsComponent } from './as/as.component';
import { InfoComponent } from './as/info/info.component';

import { JsonService } from './json.service';
import { InfoResolveService } from './as/info/info-resolve.service';

@NgModule({
  declarations: [
    AppComponent,
    IndexComponent,
    AsComponent,
    InfoComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    RouterModule.forRoot([
      {
        path : '',
        component : IndexComponent
      },{
        path : 'as',
        component : AsComponent,
        children : [
          {
            path : ':brand_name',
            component : InfoComponent,
            resolve : {
              info : InfoResolveService
            }
          }
        ]
      }
    ])
  ],
  providers: [JsonService, InfoResolveService],
  bootstrap: [AppComponent]
})
export class AppModule { }
