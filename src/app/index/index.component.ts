import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';

import { JsonService } from '../json.service';

@Component({
  selector: 'app-index',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.css']
})
export class IndexComponent implements OnInit {

  constructor(
    private jsonService : JsonService
  ) { }

  ngOnInit() {
    this.getBrandList();
    this.getKeywordList();
  }

  @ViewChild('logo') logoView;

  brand_name_list = [];
  brand_info = {};
  keyword_list = {};

  getBrandList(){
    this.jsonService.getBrandInfo()
    .subscribe(result => {
      console.log(result);
      this.brand_name_list = Object.keys(result);
      this.brand_info = result;
    });
  }

  getKeywordList(){
    this.jsonService.getKeywordList()
    .subscribe(result => {
      this.keyword_list = result;
    });
  }

  search($event){
    let keyword = $event.target.value.toLowerCase().replace(/[ ]/g, '');
    if (keyword.length > 0) {
      this.logoView.nativeElement.className = 'hidden';
      window.scrollTo(0,0);
      var target_arr = [];
      for (var idx in this.keyword_list){
        var value_arr = this.keyword_list[idx];
        for (var i = 0; i < value_arr.length; i++) {
          if(value_arr[i].match(keyword) && target_arr.indexOf(idx.toLowerCase()) < 0){
            target_arr.push(idx.toLowerCase());
          }
        }
      }
      this.brand_name_list = target_arr;
    }else{
      this.logoView.nativeElement.className = '';
      this.getBrandList();
    }
  }

}
