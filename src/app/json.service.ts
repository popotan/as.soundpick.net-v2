import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

@Injectable()
export class JsonService {

  constructor(
    private $http: Http
  ) { }

  getKeywordList(){
    return this.$http.get('/assets/json/keyword-list.json')
    .map((res: Response) => res.json())
  }

  getBrandInfo(){
    return this.$http.get('/assets/json/brand-info.json')
    .map((res:Response) => res.json())
  }
}