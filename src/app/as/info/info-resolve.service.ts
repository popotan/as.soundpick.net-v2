import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';

import { JsonService } from '../../json.service';

@Injectable()
export class InfoResolveService implements Resolve<any> {

  constructor(
    private jsonService : JsonService
  ) { }

  resolve(route: ActivatedRouteSnapshot, State: RouterStateSnapshot):Promise<any>{
    return new Promise((resolve, reject) => {
      this.jsonService.getBrandInfo().subscribe(result => {
        let info = result[route.params['brand_name']];
        resolve(info);
      }, error => {
        reject(error);
      });
    });
  }

}
