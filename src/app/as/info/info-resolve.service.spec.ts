import { TestBed, inject } from '@angular/core/testing';

import { InfoResolveService } from './info-resolve.service';

describe('InfoResolveService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [InfoResolveService]
    });
  });

  it('should ...', inject([InfoResolveService], (service: InfoResolveService) => {
    expect(service).toBeTruthy();
  }));
});
