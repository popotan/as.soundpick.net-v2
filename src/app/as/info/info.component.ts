import { DomSanitizer } from '@angular/platform-browser';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { JsonService } from '../../json.service';
@Component({
  selector: 'app-info',
  templateUrl: './info.component.html',
  styleUrls: ['./info.component.css']
})
export class InfoComponent implements OnInit {

  constructor(
    private route: ActivatedRoute,
    private jsonService : JsonService,
    private sanitizer : DomSanitizer
  ) { }

  ngOnInit() {
    this.route.data.subscribe(data => {
      this.info = data['info'];
    });
  }

  info;

  tel_split(tel){
    let arr = [];
    if(/^02/.test(tel)){
      arr.push(tel.substr(0,2));
      arr.push(tel.substr(2,4));
      arr.push(tel.substr(6,4));
    }else if(/^031/.test(tel)){
      arr.push(tel.substr(0,3));
      arr.push(tel.substr(3,3));
      arr.push(tel.substr(6,4));
    }else{
      arr.push(tel.substr(0,3));
      arr.push(tel.substr(3,4));
      arr.push(tel.substr(7,4));
    }
    let r = arr.join('<br>');
    return this.sanitizer.bypassSecurityTrustHtml(r);
  }
}