import { As.Soundpick.NetV2Page } from './app.po';

describe('as.soundpick.net-v2 App', () => {
  let page: As.Soundpick.NetV2Page;

  beforeEach(() => {
    page = new As.Soundpick.NetV2Page();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
